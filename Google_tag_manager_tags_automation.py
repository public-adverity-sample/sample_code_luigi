from google.oauth2 import service_account
from googleapiclient import discovery
from googleapiclient.errors import HttpError
from pprint import pprint
import time 
SCOPES = ['https://www.googleapis.com/auth/tagmanager.edit.containers', 'https://www.googleapis.com/auth/tagmanager.readonly']
SERVICE_ACCOUNT_FILE = '/Users/luigilongo/Downloads/omnia-gtm-4ab6c07670dd.json'
credentials = service_account.Credentials.from_service_account_file(
SERVICE_ACCOUNT_FILE,scopes=SCOPES)
gtm = discovery.build('tagmanager', 'v2',credentials=credentials)
time.sleep(10)
account_id = gtm.accounts().list().execute()
account_id_value = account_id['account'][0]['path']
time.sleep(10)
conteiner_id = gtm.accounts().containers().list(parent=account_id_value).execute()
conteiner_id_value= conteiner_id['container'][0]['path']
time.sleep(10)
workspace_id = gtm.accounts().containers().workspaces().list(parent=conteiner_id_value).execute()
workspace_id_value = workspace_id['workspace'][0]['path']
account_path = workspace_id_value
time.sleep(10)
folders = gtm.accounts().containers().workspaces().folders().list(parent= account_path).execute()
#pprint(folders)
triggers_ecommerce_folder = folders['folder'][0]['folderId']
dataLayer_notation_folder = folders['folder'][2]['folderId']
dataLayer_ecommerce_folder = folders['folder'][3]['folderId']
facebook_folder = folders['folder'][5]['folderId']
adwords_folder = folders['folder'][6]['folderId']
track_ga_folder = folders['folder'][4]['folderId']

time.sleep(10)
trigger_list = gtm.accounts().containers().workspaces().triggers().list(parent= account_path).execute()
#pprint(trigger_list)
trigger_add_to_cart = trigger_list['trigger'][0]['triggerId']
trigger_categoryViewEvent = trigger_list['trigger'][1]['triggerId']
trigger_checkout_allSteps = trigger_list['trigger'][2]['triggerId']
trigger_pageview = trigger_list['trigger'][3]['triggerId']
triggeer_productViewEvent =  trigger_list['trigger'][4]['triggerId']
triggeer_removeFromCart =  trigger_list['trigger'][5]['triggerId']
triggeer_checkout_step1_cart =  trigger_list['trigger'][6]['triggerId']
triggeer_checkout_step2_shipping =  trigger_list['trigger'][7]['triggerId']
triggeer_checkout_step3_payment =  trigger_list['trigger'][8]['triggerId']
triggeer_checkout_step4_review =  trigger_list['trigger'][9]['triggerId']
triggeer_checkout_step5_confirmation =  trigger_list['trigger'][10]['triggerId']
triggeer_scrollDepth =  trigger_list['trigger'][11]['triggerId']
triggeer_engage_videoview_percent =  trigger_list['trigger'][12]['triggerId']
triggeer_lead_call =  trigger_list['trigger'][13]['triggerId']
triggeer_lead_form =  trigger_list['trigger'][14]['triggerId']
triggeer_lead_mailto =  trigger_list['trigger'][15]['triggerId'] 

time.sleep(30)
FB_AddToCart = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
  {'name': 'FB - AddToCart', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': trigger_add_to_cart, 
  'tagFiringOption': 'oncePerEvent', 
  'parentFolderId':facebook_folder, 
  'parameter': [{'key': 'html',
                'type': 'template',
                'value': '<script>\n'
                         "  fbq('track', 'AddToCart', {\n"
                         "    content_type: 'product',\n"
                         '    content_ids: [{{add.products.0.id}}],\n'
                         '    value: {{add.products.0.price}},\n'
                         '    currency: {{currencyCode}}\n'
                         '  });\n'
                         '</script>'},
               {'key': 'supportDocumentWrite',
                'type': 'boolean',
                'value': 'false'}], 'type': 'html'}).execute()
time.sleep(30)

FB_All_Pages = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
  {'name': 'FB - All Pages', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': trigger_pageview, 
  'tagFiringOption': 'oncePerEvent', 'parentFolderId':facebook_folder,  
  'parameter': [{'key': 'html',
                'type': 'template',
                         'value': '<!-- Facebook Pixel Code -->\n'
                                  '<script>\n'
                                  '  !function(f,b,e,v,n,t,s)\n'
                                  '  '
                                  '{if(f.fbq)return;n=f.fbq=function(){n.callMethod?\n'
                                  '  '
                                  'n.callMethod.apply(n,arguments):n.queue.push(arguments)};\n'
                                  '  '
                                  "if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';\n"
                                  '  '
                                  'n.queue=[];t=b.createElement(e);t.async=!0;\n'
                                  '  t.src=v;s=b.getElementsByTagName(e)[0];\n'
                                  '  s.parentNode.insertBefore(t,s)}(window, '
                                  "document,'script',\n"
                                  '  '
                                  "'https://connect.facebook.net/en_US/fbevents.js');\n"
                                  "  fbq('init', '{{Facebook Pixel - INSERIRE "
                                  "ID}}');\n"
                                  "  fbq('track', 'PageView');\n"
                                  '</script>\n'
                                  '<noscript>\n'
                                  '  <img height="1" width="1" '
                                  'style="display:none" \n'
                                  '       '
                                  'src="https://www.facebook.com/tr?id={{Facebook '
                                  'Pixel - INSERIRE '
                                  'ID}}&ev=PageView&noscript=1"/>\n'
                                  '</noscript>\n'
                                  '<!-- End Facebook Pixel Code -->'},
               {'key': 'supportDocumentWrite',
                'type': 'boolean',
                'value': 'false'}], 'type': 'html'}).execute()
time.sleep(30)

FB_Checkout_step2_shipping = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
  {'name': 'FB - Checkout_step2_shipping', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_checkout_step2_shipping, 
  'tagFiringOption': 'oncePerEvent', 'parentFolderId':facebook_folder,  
          'parameter': [{'key': 'html',
                         'type': 'template',
                         'value': ' <script>\n'
                                  "  fbq('trackCustom', 'Shipping');\n"
                                  ' </script>'},
                        {'key': 'supportDocumentWrite',
                         'type': 'boolean',
                         'value': 'false'}], 'type': 'html'}).execute()
time.sleep(30)


FB_Checkout_step3_payment = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
  {'name': 'FB - Checkout_step3_payment', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_checkout_step3_payment, 
  'tagFiringOption': 'oncePerEvent', 'parentFolderId':facebook_folder,  
          'parameter': [{'key': 'html',
                         'type': 'template',
                         'value': ' <script>\n'
                                  "  fbq('trackCustom', 'Payment');\n"
                                  ' </script>'},
                        {'key': 'supportDocumentWrite',
                         'type': 'boolean',
                         'value': 'false'}], 'type': 'html'}).execute()


time.sleep(30)

FB_Checkout_step4_review = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
  {'name': 'FB - Checkout_step4_review', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_checkout_step4_review, 
  'tagFiringOption': 'oncePerEvent', 'parentFolderId':facebook_folder,  
          'parameter': [{'key': 'html',
                         'type': 'template',
                         'value': ' <script>\n'
                                  "  fbq('trackCustom', 'Review');\n"
                                  ' </script>'},
                        {'key': 'supportDocumentWrite',
                         'type': 'boolean',
                         'value': 'false'}], 'type': 'html'}).execute()

time.sleep(30)

FB_Engage_pageScroll = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
  {'name': 'FB - Engage_pageScroll', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_scrollDepth, 
  'tagFiringOption': 'oncePerEvent', 'parentFolderId':facebook_folder,  
          'parameter': [{'key': 'html',
                         'type': 'template',
                         'value': ' <script>\n'
                                  "  fbq('trackCustom', 'engage', "
                                  "{engage_type: 'pageScroll'});\n"
                                  ' </script>'},
                        {'key': 'supportDocumentWrite',
                         'type': 'boolean',
                         'value': 'false'}], 'type': 'html'}).execute()
time.sleep(30)

FB_Engage_videoView = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
  {'name': 'FB - Engage_videoView', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_engage_videoview_percent, 
  'tagFiringOption': 'oncePerEvent', 'parentFolderId':facebook_folder,  
          'parameter': [{'key': 'html',
                         'type': 'template',
                         'value': ' <script>\n'
                                  "  fbq('trackCustom', 'engage', "
                                  "{engage_type: 'videoView', value: '{{Video "
                                  "Percent}}'});\n"
                                  ' </script>'},
                        {'key': 'supportDocumentWrite',
                         'type': 'boolean',
                         'value': 'false'}],'type': 'html'}).execute()

time.sleep(20)

FB_InitiateCheckout = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
  {'name': 'FB - InitiateCheckout', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_checkout_step1_cart, 
  'tagFiringOption': 'oncePerEvent', 'parentFolderId':facebook_folder, 
          'parameter': [{'key': 'html',
                         'type': 'template',
                         'value': '<script>\n'
                                  "  fbq('track', 'InitiateCheckout', {\n"
                                  "    content_type: 'product',\n"
                                  '    content_ids: '
                                  '{{checkout.products.idList}},\n'
                                  '    value: {{checkoutTotalCart}},\n'
                                  '    currency: {{currencyCode}}\n'
                                  '  });\n'
                                  '</script>'},
                        {'key': 'supportDocumentWrite',
                         'type': 'boolean',
                         'value': 'false'}], 'type': 'html'}).execute()

time.sleep(30)

FB_Lead = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
  {'name': 'FB - Lead', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': [triggeer_lead_call, triggeer_lead_form, triggeer_lead_mailto], 
  'tagFiringOption': 'oncePerEvent', 'parentFolderId':facebook_folder, 
          'parameter': [{'key': 'html',
                         'type': 'template',
                         'value': ' <script>\n'
                                  "   fbq('track', 'Lead');\n"
                                  ' </script>'},
                        {'key': 'supportDocumentWrite',
                         'type': 'boolean',
                         'value': 'false'}], 'type': 'html'}).execute()
time.sleep(30)

FB_Purchase = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
  {'name': 'FB - Purchase', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_checkout_step5_confirmation, 'tagFiringOption': 'oncePerEvent', 'parentFolderId':facebook_folder, 
          'parameter': [{'key': 'html',
                         'type': 'template',
                         'value': '<script>\n'
                                  "  fbq('track', 'Purchase', {\n"
                                  "    content_type: 'product',\n"
                                  '    content_ids: '
                                  '{{purchase.products.idList}},\n'
                                  '    value: '
                                  '{{purchase.actionField.revenue}},\n'
                                  '    currency: {{currencyCode}}\n'
                                  '  });\n'
                                  '</script>'},
                        {'key': 'supportDocumentWrite',
                         'type': 'boolean',
                         'value': 'false'}], 'type': 'html'}).execute()

time.sleep(30)

FB_ViewContent = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
  {'name': 'FB - ViewContent', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_productViewEvent, 'tagFiringOption': 'oncePerEvent', 'parentFolderId':facebook_folder, 
          'parameter': [{'key': 'html',
                         'type': 'template',
                         'value': '<!-- Facebook Pixel Code -->\n'
                                  '<script>  \n'
                                  "  fbq('track', 'ViewContent', {\n"
                                  "        content_type: 'product',\n"
                                  '        content_ids: '
                                  '[{{detail.products.0.id}}],\n'
                                  '        value: '
                                  '{{detail.products.0.price}},\n'
                                  '        currency: {{currencyCode}}\n'
                                  '      });\n'
                                  '</script>\n'
                                  '<!-- End Facebook Pixel Code -->'},
                        {'key': 'supportDocumentWrite',
                         'type': 'boolean',
                         'value': 'false'}],'type': 'html'}).execute()
time.sleep(30)

GoogleADS_Purchase_Conversion_Tag = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
{'name': 'GoogleADS - Purchase Conversion Tag', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_checkout_step5_confirmation, 'tagFiringOption': 'oncePerEvent', 'parentFolderId':adwords_folder, 
          'parameter': [{'key': 'enableConversionLinker',
                         'type': 'boolean',
                         'value': 'true'},
                        {'key': 'enableProductReporting',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'orderId',
                         'type': 'template',
                         'value': '{{purchase.actionField.id}}'},
                        {'key': 'conversionValue',
                         'type': 'template',
                         'value': '{{purchase.actionField.revenue}}'},
                        {'key': 'conversionCookiePrefix',
                         'type': 'template',
                         'value': '_gcl'},
                        {'key': 'conversionId',
                         'type': 'template',
                         'value': '{{GoogleADS Remarketing - INSERIRE ID ''REMARKETING}}'},
                        {'key': 'currencyCode',
                         'type': 'template',
                         'value': '{{currencyCode}}'},
                        {'key': 'conversionLabel',
                         'type': 'template',
                         'value': '{{GoogleADS Lead Conversion - INSERIRE ''ETICHETTA CONVERSIONE}}'},
                        {'key': 'rdp', 
                        'type': 'boolean', 
                        'value': 'false'}],'type': 'awct'}).execute()
time.sleep(30)

GoogleADS_Remarketing_Tag = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
{'name': 'GoogleADS - Remarketing Tag', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': trigger_pageview, 'tagFiringOption': 'oncePerEvent', 'parentFolderId':adwords_folder, 
          'parameter': [{'key': 'enableDynamicRemarketing',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'conversionId',
                         'type': 'template',
                         'value': '{{GoogleADS Remarketing - INSERIRE ID ''REMARKETING}}'},
                        {'key': 'customParamsFormat',
                         'type': 'template',
                         'value': 'NONE'},
                        {'key': 'rdp', 'type': 'boolean', 'value': 'false'}],'type': 'sp'}).execute()
time.sleep(30)

UA_addToCart= gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
{'name': 'UA - addToCart', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': trigger_add_to_cart, 'tagFiringOption': 'oncePerEvent', 'parentFolderId':track_ga_folder, 
          'parameter': [{'key': 'nonInteraction',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'overrideGaSettings',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'eventValue',
                         'type': 'template',
                         'value': '{{add.products.0.price}}'},
                        {'key': 'eventCategory',
                         'type': 'template',
                         'value': 'Ecommerce'},
                        {'key': 'trackType',
                         'type': 'template',
                         'value': 'TRACK_EVENT'},
                        {'key': 'gaSettings',
                         'type': 'template',
                         'value': '{{Universal Analytics - settings}}'},
                        {'key': 'eventAction',
                         'type': 'template',
                         'value': 'Add to Cart'},
                        {'key': 'eventLabel',
                         'type': 'template',
                         'value': '{{add.products.0.id}}'}],'type': 'ua'}).execute()
time.sleep(30)

UA_checkout = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
{'name': 'UA - checkout', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': trigger_checkout_allSteps, 'tagFiringOption': 'oncePerEvent', 'parentFolderId':track_ga_folder, 
          'parameter': [{'key': 'nonInteraction',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'overrideGaSettings',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'eventValue',
                         'type': 'template',
                         'value': '{{checkoutTotalCart}}'},
                        {'key': 'eventCategory',
                         'type': 'template',
                         'value': 'Ecommerce'},
                        {'key': 'trackType',
                         'type': 'template',
                         'value': 'TRACK_EVENT'},
                        {'key': 'gaSettings',
                         'type': 'template',
                         'value': '{{Universal Analytics - settings}}'},
                        {'key': 'eventAction',
                         'type': 'template',
                         'value': 'Checkout'},
                        {'key': 'eventLabel',
                         'type': 'template',
                         'value': '{{checkout.step}}'}],'type': 'ua'}).execute()
time.sleep(30)

UA_engage_pagescroll = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
{'name': 'UA - engage_pagescroll', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_scrollDepth, 'tagFiringOption': 'oncePerEvent', 'parentFolderId':track_ga_folder, 
          'parameter': [{'key': 'nonInteraction',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'overrideGaSettings',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'eventCategory',
                         'type': 'template',
                         'value': 'engage'},
                        {'key': 'trackType',
                         'type': 'template',
                         'value': 'TRACK_EVENT'},
                        {'key': 'gaSettings',
                         'type': 'template',
                         'value': '{{Universal Analytics - settings}}'},
                        {'key': 'eventAction',
                         'type': 'template',
                         'value': 'pagescroll'},
                        {'key': 'eventLabel',
                         'type': 'template',
                         'value': '{{Page Path}}'}],'type': 'ua'}).execute()

time.sleep(30)


UA_engage_videoview = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
{'name': 'UA - engage_videoview', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_engage_videoview_percent, 'tagFiringOption': 'oncePerEvent', 'parentFolderId':track_ga_folder, 
          'parameter': [{'key': 'nonInteraction',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'overrideGaSettings',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'eventValue',
                         'type': 'template',
                         'value': '{{Video Percent}}'},
                        {'key': 'eventCategory',
                         'type': 'template',
                         'value': 'engage'},
                        {'key': 'trackType',
                         'type': 'template',
                         'value': 'TRACK_EVENT'},
                        {'key': 'gaSettings',
                         'type': 'template',
                         'value': '{{Universal Analytics - settings}}'},
                        {'key': 'eventAction',
                         'type': 'template',
                         'value': 'videoview'},
                        {'key': 'eventLabel',
                         'type': 'template',
                         'value': '{{Video Title}}'}],'type': 'ua'}).execute()

time.sleep(30)

UA_lead_call = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
{'name': 'UA - lead_call', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_lead_call, 'tagFiringOption': 'oncePerEvent', 'parentFolderId':track_ga_folder, 
          'parameter': [{'key': 'nonInteraction',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'overrideGaSettings',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'eventCategory',
                         'type': 'template',
                         'value': 'lead'},
                        {'key': 'trackType',
                         'type': 'template',
                         'value': 'TRACK_EVENT'},
                        {'key': 'gaSettings',
                         'type': 'template',
                         'value': '{{Universal Analytics - settings}}'},
                        {'key': 'eventAction',
                         'type': 'template',
                         'value': 'call'},
                        {'key': 'eventLabel',
                         'type': 'template',
                         'value': '{{Page URL}}'}],'type': 'ua'}).execute()

time.sleep(30)


UA_lead_form = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
{'name': 'UA - lead_form', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_lead_form, 'tagFiringOption': 'oncePerEvent', 'parentFolderId':track_ga_folder, 
          'parameter': [{'key': 'nonInteraction',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'overrideGaSettings',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'eventCategory',
                         'type': 'template',
                         'value': 'lead'},
                        {'key': 'trackType',
                         'type': 'template',
                         'value': 'TRACK_EVENT'},
                        {'key': 'gaSettings',
                         'type': 'template',
                         'value': '{{Universal Analytics - settings}}'},
                        {'key': 'eventAction',
                         'type': 'template',
                         'value': 'form'},
                        {'key': 'eventLabel',
                         'type': 'template',
                         'value': '{{Page URL}}'}],'type': 'ua'}).execute()

time.sleep(30)


UA_lead_mail = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
{'name': 'UA - lead_mail', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_lead_mailto, 'tagFiringOption': 'oncePerEvent', 'parentFolderId':track_ga_folder, 
          'parameter': [{'key': 'nonInteraction',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'overrideGaSettings',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'eventCategory',
                         'type': 'template',
                         'value': 'lead'},
                        {'key': 'trackType',
                         'type': 'template',
                         'value': 'TRACK_EVENT'},
                        {'key': 'gaSettings',
                         'type': 'template',
                         'value': '{{Universal Analytics - settings}}'},
                        {'key': 'eventAction',
                         'type': 'template',
                         'value': 'mail'},
                        {'key': 'eventLabel',
                         'type': 'template',
                         'value': '{{Page URL}}'}],'type': 'ua'}).execute()

time.sleep(30)


UA_pageview_impressions_detail_purchase = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
{'name': 'UA - pageview - impressions - detail - purchase', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': trigger_pageview, 'tagFiringOption': 'oncePerEvent', 'parentFolderId':track_ga_folder, 
          'parameter': [{'key': 'overrideGaSettings',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'trackType',
                         'type': 'template',
                         'value': 'TRACK_PAGEVIEW'},
                        {'key': 'gaSettings',
                         'type': 'template',
                         'value': '{{Universal Analytics - settings}}'}],'type': 'ua'}).execute()

time.sleep(30)


UA_removeFromCart = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = 
{'name': 'UA - removeFromCart', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': triggeer_removeFromCart, 'tagFiringOption': 'oncePerEvent', 'parentFolderId':track_ga_folder, 
          'parameter': [{'key': 'nonInteraction',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'overrideGaSettings',
                         'type': 'boolean',
                         'value': 'false'},
                        {'key': 'eventValue',
                         'type': 'template',
                         'value': '{{remove.products.0.price}}'},
                        {'key': 'eventCategory',
                         'type': 'template',
                         'value': 'Ecommerce'},
                        {'key': 'trackType',
                         'type': 'template',
                         'value': 'TRACK_EVENT'},
                        {'key': 'gaSettings',
                         'type': 'template',
                         'value': '{{Universal Analytics - settings}}'},
                        {'key': 'eventAction',
                         'type': 'template',
                         'value': 'Remove from Cart'},
                        {'key': 'eventLabel',
                         'type': 'template',
                         'value': '{{remove.products.0.id}}'}],'type': 'ua'}).execute()
#OK_iubenda_cookie_consent = gtm.accounts().containers().workspaces().tags().create(parent = account_path, body = {'name': '01_OK_iubenda_cookie_consent', 'monitoringMetadata': {'type': 'map'},'firingTriggerId': ['200'], 'tagFiringOption': 'oncePerEvent', 'priority': {'type': 'integer', 'value': '99999'}, 'parentFolderId':21, 'parameter': [{'key': 'html',
#                         'type': 'template',
#                         'value': '<script type="text/javascript">\n'
#                                  'var _iub = _iub || [];\n'
#                                  '_iub.csConfiguration = {\n'
#                                  '  "countryDetection":true,\n'
#                                  '  "consentOnContinuedBrowsing":false,\n'
#                                  '  "lang":"it",\n'
#                                  '  "siteId":{{01_OK_siteId_iubenda}},\n'
#                                  '  '
#                                  '"cookiePolicyId":{{01_OK_cookiePolicyId_iubenda}}, \n'
#                                  '  "banner": {          \n'
#                                  '    "acceptButtonDisplay":true,\n'
#                                  '    "customizeButtonDisplay":true,\n'
#                                  '    "acceptButtonColor":"#0073CE",\n'
#                                  '    "acceptButtonCaptionColor":"white",\n'
#                                  '    "customizeButtonColor":"#DADADA",\n'
#                                  '    '
#                                  '"customizeButtonCaptionColor":"#4D4D4D",\n'
#                                 '    "rejectButtonColor":"#0073CE",\n'
#                                  '    "rejectButtonCaptionColor":"white",\n'
#                                  '    "position":"bottom",\n'
#                                  '    "textColor":"black",\n'
#                                  '    "backgroundColor":"white" \n'
#                                  '  },\n'
#                                  '  "callback": {\n'
#                                  '            '
#                                  'onPreferenceExpressedOrNotNeeded: '
#                                  'function(preference) {\n'
#                                  '                dataLayer.push({\n'
#                                  '                    iubenda_ccpa_opted_out: '
#                                  '_iub.cs.api.isCcpaOptedOut()\n'
#                                  '                });\n'
#                                  '                if (!preference) {\n'
#                                  '                    dataLayer.push({\n'
#                                  '                        event: '
#                                  '"iubenda_preference_not_needed"\n'
#                                  '                    });\n'
#                                  '                } else {\n'
#                                  '                    if (preference.consent '
#                                  '=== true) {\n'
#                                  '                        dataLayer.push({\n'
#                                  '                            event: '
#                                  '"iubenda_consent_given"\n'
#                                  '                        });\n'
#                                  '                        '
#                                  'console.log("iubenda_consent_given");\n'
#                                  '                    } else if '
#                                  '(preference.consent === false) {\n'
#                                  '                        dataLayer.push({\n'
#                                  '                            event: '
#                                  '"iubenda_consent_rejected"\n'
#                                  '                        });\n'
#                                  '                    } else if '
#                                  '(preference.purposes) {\n'
#                                  '                        for (var purposeId '
#                                  'in preference.purposes) {\n'
#                                  '                            if '
#                                  '(preference.purposes[purposeId]) {\n'
#                                  '                                '
#                                  'dataLayer.push({\n'
#                                  '                                    event: '
#                                  '"iubenda_consent_given_purpose_" + '
#                                  'purposeId\n'
#                                  '                                });\n'
#                                  '                            }\n'
#                                  '                        }\n'
#                                  '                    }\n'
#                                  '                }\n'
#                                  '            }\n'
#                                  '        }};\n'
#                                  '</script>\n'
#                                  '<script type="text/javascript" '
#                                  'src="//cdn.iubenda.com/cs/iubenda_cs.js" '
#                                  'charset="UTF-8" async></script>'},
#                        {'key': 'supportDocumentWrite',
#                         'type': 'boolean',
#                         'value': 'false'}],'type': 'html'}).execute()


















