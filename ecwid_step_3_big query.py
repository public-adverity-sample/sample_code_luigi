from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
import pandas as pd
import pandas_gbq
import google.auth
credentials, project_id = google.auth.default()
table_id = 'ecw_data_collection.ecwid_raw_data_all_collection'
dfa = pd.read_pickle('/home/alessandro_longo1986/ecwid_step_2/ecwid_2.pickle')
def import_bg():
	pandas_gbq.to_gbq(dfa, table_id, project_id=project_id, if_exists='append')

dag = DAG('ecw_third_call', description='thir_block_ecwid',
          schedule_interval='25 6 * * *',
          start_date=datetime(2017, 3, 20), catchup=False)
ecwid_operator = PythonOperator(task_id='ecwid_task_bq', python_callable=import_bg, dag=dag)