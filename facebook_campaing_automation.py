
import requests
import pandas as pd
from facebook_business.api import FacebookAdsApi
from facebook_business import adobjects
from facebook_business.adobjects.adaccountuser import AdAccountUser
from facebook_business.adobjects.campaign import Campaign
from facebook_business.adobjects.adaccount import AdAccount
from time import sleep
import facebook
permanent_token='xxxxxxxx'
FacebookAdsApi.init(access_token=permanent_token)
me = AdAccountUser(fbid='me')
adAccounts = list(me.get_ad_accounts(['name']))
account_name = []
for value in adAccounts:
	account_name.append(value['name'])
index = account_name.index('Botteghe Italiane')
botteghe = adAccounts[index]['id']
campaign_catalog = ['Conversion_Retargeting_Vendite_Catalogo_countryCH_languageIT_ageALL_genderALL_industryFOOD_mimic']
campaign_consideration = ['Consideration_Prospecting_Traffico_al_sito_countryCH_languageIT_ageALL_genderALL_industryFOOD_mimic']
campaign_reach = ['Brand_Awareness_Piano_Editoriale_countryCH_languageIT_ageALL_genderALL_industryFOOD_mimic']
campaign_drive = ['Drive_To_store_[countryCH_languageIT_ageALL_genderALL_industryFOOD]_mimic']
goal_naming_catalog = ['PRODUCT_CATALOG_SALES']
goal_naming_consideration = ['LINK_CLICKS']
goal_naming_reach = ['REACH']
goal_naming_store = ['STORE_VISITS']
def catalogue(x,y):
	for (a,b) in zip (campaign_catalog,goal_naming_catalog):
		fields = [
		]
		params = {
		'name': a,
		'objective': b,
		'status': 'PAUSED',
		'bid_strategy':'LOWEST_COST_WITHOUT_CAP',
		'special_ad_category':'NONE',
		'daily_budget': 100,
		'promoted_object':{'product_catalog_id':'934342263694513'}
		}
		return AdAccount(botteghe).create_campaign(fields=fields,params=params)

catologue_result = catalogue(campaign_catalog, goal_naming_catalog)
print(catologue_result)

def consideration(x,y):
	for (a,b) in zip (campaign_consideration,goal_naming_consideration):
		fields = [
		]
		params = {
		'name': a,
	  	'objective': b,
		'status': 'PAUSED',
		'bid_strategy':'LOWEST_COST_WITHOUT_CAP',
		'special_ad_category':'NONE',
		'daily_budget': 100,
		}
		return AdAccount(botteghe).create_campaign(fields=fields,params=params)

consideration_result= consideration(campaign_consideration,goal_naming_consideration)
print(consideration_result)

def reach(x,y):
	for (a,b) in zip (campaign_reach,goal_naming_reach):
		fields = [
		]
		params = {
		'name': a,
		'objective': b,
		'status': 'PAUSED',
		#'bid_strategy':'LOWEST_COST_WITHOUT_CAP',
		'special_ad_category':'NONE',
		#'daily_budget': 100,
		}
		return AdAccount(botteghe).create_campaign(fields=fields,params=params)

reach_result = reach(campaign_reach,goal_naming_reach)
print(reach_result)

def store(x,y):
	for (a,b) in zip (campaign_drive,goal_naming_store):
		fields = [
		]
		params = {
		'name': a,
		'objective': b,
		'status': 'PAUSED',
		'bid_strategy':'LOWEST_COST_WITHOUT_CAP',
		'special_ad_category':'NONE',
		'daily_budget': 100,
		'promoted_object':{'page_id': '1235945156461536'}
		}
		return AdAccount(botteghe).create_campaign(fields=fields,params=params)
store_result = store(campaign_drive,goal_naming_store)
print(reach_result)
