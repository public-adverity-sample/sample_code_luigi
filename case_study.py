import requests
from pprint import pprint
import time
from datetime import datetime
repository_insights_git_api = ['code_frequency', 'contributors','participation','punch_card']
endpoint_stats = ['stats','stats','stats','stats']
issues_endpoint = ['issues']
headers = {"Authorization": "'Content-Type': 'application/json; charset=utf-8', Accept: application/vnd.github.v3+json, username xxxxxx, Bearer xxxxxxx"}
product_data = []

def get_data_product_team(a,b):
    global product_data
    for(y,x) in zip(a,b):
        github_request = requests.get('https://api.github.com/repos/pmedianetwork/webpack-config/'+y+'/'+x, headers=headers)
        time.sleep(10)
        # implenting checking control mechianism based on Github requests architecture.
        if github_request.status_code == 200:
            check_header= github_request.headers
            request_remaining = check_header['X-RateLimit-Remaining']
            reset_request_time = check_header['X-RateLimit-Reset']
            request_remaining_int = int(request_remaining)
            reset_request_time_int = int(reset_request_time)
            user_retry = datetime.utcfromtimestamp(reset_request_time_int).strftime('%Y-%m-%d %H:%M:%S')
            if request_remaining_int >10:
                response =  github_request.json()
                print(response)
                product_data.append(response)
            else:
                raise Exception('Limit of  request approaching, you will be able to retry at'+ ' '+user_retry)
        elif github_request.status_code == 202:
            time.sleep(10)
            print('Data no cached for your query, a background job is also fired to start compiling these statistics. Please give the job a few moments to complete')
        else:
            print('Some other errors occured!')
get_data_product_team(endpoint_stats, repository_insights_git_api)


def get_issue_data(a):
    global product_data
    for y in a:
        github_request = requests.get('https://api.github.com/repos/pmedianetwork/webpack-config/'+y, headers=headers)
        time.sleep(10)
        # implenting checking control mechianism based on Github requests architecture.
        if github_request.status_code == 200:
            check_header= github_request.headers
            request_remaining = check_header['X-RateLimit-Remaining']
            reset_request_time = check_header['X-RateLimit-Reset']
            request_remaining_int = int(request_remaining)
            reset_request_time_int = int(reset_request_time)
            user_retry = datetime.utcfromtimestamp(reset_request_time_int).strftime('%Y-%m-%d %H:%M:%S')
            if request_remaining_int < 58:
                response =  github_request.json()
                print(response)
                product_data.append(response)
            else:
                raise Exception('Limit of  request approaching, you will be able to retry at'+ ' '+user_retry)
        elif github_request.status_code == 202:
            time.sleep(10)
            print('Data no cached for your query, a background job is also fired to start compiling these statistics. Please give the job a few moments to complete')
        else:
            print('Some other errors occured!')
get_issue_data(issues_endpoint)

