from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
import requests
from pprint import pprint
import pandas as pd
import pickle
dfa = pd.read_pickle('/home/alessandro_longo1986/ecwid_step_1/step_1.pickle')
def start_yesterday():
	from datetime import datetime, date, time, timedelta
	dt = datetime.combine(date.today(), time(0, 0, 0))
	sday_timestamp = None
	sday_timestamp = int((dt - timedelta(days=1)).timestamp())
	return sday_timestamp

lower_bound = start_yesterday()

def end_yesterday():
	from datetime import datetime, date, time, timedelta
	dt = datetime.combine(date.today(), time(0, 0, 0))
	eday_timestamp = None
	eday_timestamp = int((dt - timedelta(seconds=1)).timestamp())
	return eday_timestamp

upbound = end_yesterday()

def yesterda_dfa():
	from datetime import datetime, timedelta
	yesay_date = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')
	return yesay_date

yesterday = yesterda_dfa() 
payload = {'createdFrom': lower_bound ,'createdTo':upbound, 'token':'xxxxxx'}
customer_call = requests.get('https://app.ecwid.com/api/v3/30601136/orders?', payload)
dictionary_json = customer_call.json()
x_mail = dictionary_json['items']
type(x_mail)
len(x_mail)
mail = []
i=0
while i<len(x_mail):
	mail.append(x_mail[i]['email'])
	i+=1

product_per_client = []
i=0
while i<len(x_mail):
	occurence = x_mail[i]['items']
	product_per_client.append(sum([1 for d in occurence if 'id' in d]))
	i+=1

len(product_per_client)

customerId =[]
i=0
while i<len(x_mail):
	customerId.append(x_mail[i]['customerId'])
	i+=1

id_client = sum([[s] * n for s, n in zip(customerId, product_per_client)], [])

billingPerson = []
i=0
while i<len(x_mail):
	billingPerson.append(x_mail[i]['billingPerson'])
	i+=1

firstname = [d['firstName'] for d in billingPerson if 'firstName' in d]
lastName = [d['lastName'] for d in billingPerson if 'lastName' in d]
companyName = [d['companyName'] for d in billingPerson if 'companyName' in d]
street = [d['street'] for d in billingPerson if 'street' in d]
city = [d['city'] for d in billingPerson if 'city' in d]
countryCode = [d['countryCode'] for d in billingPerson if 'countryCode' in d]
countryName = [d['countryName'] for d in billingPerson if 'countryName' in d]
postalCode = [d['postalCode'] for d in billingPerson if 'postalCode' in d]
stateOrProvinceCode = [d['stateOrProvinceCode'] for d in billingPerson if 'stateOrProvinceCode' in d]
stateOrProvinceName = [d['stateOrProvinceName'] for d in billingPerson if 'stateOrProvinceName' in d]
phone = [d['phone'] for d in billingPerson if 'phone' in d]

firstname = sum([[s] * n for s, n in zip(firstname, product_per_client)], [])
dfa['firstName']= firstname
lastName = sum([[s] * n for s, n in zip(lastName, product_per_client)], [])
dfa['lastName']= lastName
companyName = sum([[s] * n for s, n in zip(companyName, product_per_client)], [])
dfa['companyName']= companyName
street = sum([[s] * n for s, n in zip(street, product_per_client)], [])
dfa['street']= street
city = sum([[s] * n for s, n in zip(city, product_per_client)], [])
dfa['city']= city
countryCode = sum([[s] * n for s, n in zip(countryCode, product_per_client)], [])
dfa['countryCode']= countryCode
countryName = sum([[s] * n for s, n in zip(countryName, product_per_client)], [])
dfa['countryName']= countryName
postalCode = sum([[s] * n for s, n in zip(postalCode, product_per_client)], [])
dfa['postalCode']= postalCode
stateOrProvinceCode = sum([[s] * n for s, n in zip(stateOrProvinceCode, product_per_client)], [])
dfa['stateOrProvinceCode']= stateOrProvinceCode
stateOrProvinceName = sum([[s] * n for s, n in zip(stateOrProvinceName, product_per_client)], [])
dfa['stateOrProvinceName']= stateOrProvinceName
phone = sum([[s] * n for s, n in zip(phone, product_per_client)], [])
dfa['phone']= phone
dfa['deal_status']= 'won'
mailing = sum([[s] * n for s, n in zip(mail, product_per_client)], [])
dfa['mail']= mailing
id_client = sum([[s] * n for s, n in zip(customerId, product_per_client)], [])
dfa['id_client'] = id_client
def export_data_second_block():
	dfa.to_pickle('/home/alessandro_longo1986/ecwid_step_2/ecwid_2.pickle')
dag = DAG('ecw_second_call', description='Second_block_ecwid',
          schedule_interval='15 6 * * *',
          start_date=datetime(2017, 3, 20), catchup=False)
ecwid_operator = PythonOperator(task_id='ecwid_task', python_callable=export_data_second_block, dag=dag)

ec