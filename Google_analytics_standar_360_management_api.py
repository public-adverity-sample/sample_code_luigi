from google.oauth2 import service_account
from googleapiclient import discovery
from googleapiclient.errors import HttpError
from pprint import pprint
SCOPES = ['https://www.googleapis.com/auth/analytics','https://www.googleapis.com/auth/analytics.edit','https://www.googleapis.com/auth/analytics.readonly']
SERVICE_ACCOUNT_FILE = '/Users/luigilongo/Downloads/airflow-282719-6bc6d291a4bf.json'
credentials = service_account.Credentials.from_service_account_file(
SERVICE_ACCOUNT_FILE,scopes=SCOPES)
ga = discovery.build('analytics', 'v3',credentials=credentials)
accounts = ga.management().accounts().list().execute()
pprint(accounts)
account_id = accounts['items'][0]['id']
webproperty = ga.management().webproperties().list(accountId=account_id).execute()
pprint(webproperty)
webproperty_id = webproperty['items'][0]['id']
view = ga.management().profiles().list(accountId=account_id, webPropertyId= webproperty_id).execute()
pprint(view)
view_id = view['items'][0]['id']
ga.management().profiles().patch(accountId=account_id, webPropertyId=webproperty_id, profileId= view_id, body={'eCommerceTracking': True, 'enhancedECommerceTracking': True, 'botFilteringEnabled': True}).execute()
ga.management().goals().insert(
      accountId=account_id,
      webPropertyId=webproperty_id,
      profileId=view_id,
      body={
          'id': 1,
          'active': True,
          'name': 'addToCart',
          'type': 'Event',
          'eventDetails':{
          'useEventValue': True,
          'eventConditions': [
      {
        "type": 'ACTION',
        "matchType": 'EXACT',
        "expression": 'Add to Cart'
      },
            {
        "type": 'CATEGORY',
        "matchType": 'EXACT',
        "expression": 'Ecommerce'
      }
    ]
      }
  } 
  ).execute()

ga.management().goals().insert(
      accountId=account_id,
      webPropertyId=webproperty_id,
      profileId=view_id,
      body={
          'id': 2,
          'active': True,
          'name': 'checkoutStart',
          'type': 'Event',
          'eventDetails':{
          'useEventValue': True,
          'eventConditions': [
      {
        "type": 'ACTION',
        "matchType": 'EXACT',
        "expression": 'Checkout'
      },
      {
        "type": 'CATEGORY',
        "matchType": 'EXACT',
        "expression": 'Ecommerce'
      }, 
      {
        "type": 'LABEL',
        "matchType": 'EXACT',
        "expression": '1'
      }, 

    ]
      }
  } 
  ).execute()

ga.management().goals().insert(
      accountId=account_id,
      webPropertyId=webproperty_id,
      profileId=view_id,
      body={
          'id': 3,
          'active': True,
          'name': 'paymentMethod',
          'type': 'Event',
          'eventDetails':{
          'useEventValue': True,
          'eventConditions': [
      {
        "type": 'ACTION',
        "matchType": 'EXACT',
        "expression": 'Checkout'
      },
      {
        "type": 'CATEGORY',
        "matchType": 'EXACT',
        "expression": 'Ecommerce'
      }, 
      {
        "type": 'LABEL',
        "matchType": 'EXACT',
        "expression": '3'
      }, 

    ]
      }
  } 
  ).execute()



ga.management().goals().insert(
      accountId=account_id,
      webPropertyId=webproperty_id,
      profileId=view_id,
      body={
          'id': 6,
          'active': True,
          'name': 'lead_all',
          'type': 'Event',
          'eventDetails':{
          'useEventValue': True,
          'eventConditions': [
      {
        "type": 'CATEGORY',
        "matchType": 'EXACT',
        "expression": 'lead'
      }
    ]
      }
  } 
  ).execute()

ga.management().goals().insert(
      accountId=account_id,
      webPropertyId=webproperty_id,
      profileId=view_id,
      body={
          'id': 7,
          'active': True,
          'name': 'lead_call',
          'type': 'Event',
          'eventDetails':{
          'useEventValue': True,
          'eventConditions': [
      {
        "type": 'ACTION',
        "matchType": 'EXACT',
        "expression": 'call'
      }, 
      {
        "type": 'CATEGORY',
        "matchType": 'EXACT',
        "expression": 'lead'
      },
    ]
      }
  } 
  ).execute()

ga.management().goals().insert(
      accountId=account_id,
      webPropertyId=webproperty_id,
      profileId=view_id,
      body={
          'id': 8,
          'active': True,
          'name': 'lead_form',
          'type': 'Event',
          'eventDetails':{
          'useEventValue': True,
          'eventConditions': [
      {
        "type": 'ACTION',
        "matchType": 'EXACT',
        "expression": 'form'
      }, 
      {
        "type": 'CATEGORY',
        "matchType": 'EXACT',
        "expression": 'lead'
      },
    ]
      }
  } 
  ).execute()

ga.management().goals().insert(
      accountId=account_id,
      webPropertyId=webproperty_id,
      profileId=view_id,
      body={
          'id': 9,
          'active': True,
          'name': 'lead_mail',
          'type': 'Event',
          'eventDetails':{
          'useEventValue': True,
          'eventConditions': [
      {
        "type": 'ACTION',
        "matchType": 'EXACT',
        "expression": 'mail'
      }, 
      {
        "type": 'CATEGORY',
        "matchType": 'EXACT',
        "expression": 'lead'
      },
    ]
      }
  } 
  ).execute()

ga.management().goals().insert(
      accountId=account_id,
      webPropertyId=webproperty_id,
      profileId=view_id,
      body={
          'id': 11,
          'active': True,
          'name': 'engage_all',
          'type': 'Event',
          'eventDetails':{
          'useEventValue': True,
          'eventConditions': [
      {
        "type": 'CATEGORY',
        "matchType": 'EXACT',
        "expression": 'engage'
      }
    ]
      }
  } 
  ).execute()

ga.management().goals().insert(
      accountId=account_id,
      webPropertyId=webproperty_id,
      profileId=view_id,
      body={
          'id': 12,
          'active': True,
          'name': 'engage_pagescroll',
          'type': 'Event',
          'eventDetails':{
          'useEventValue': True,
          'eventConditions': [
      {
        "type": 'CATEGORY',
        "matchType": 'EXACT',
        "expression": 'engage'
      }, 
      {
        "type": 'ACTION',
        "matchType": 'EXACT',
        "expression": 'pagescroll'
      }
    ]
      }
  } 
  ).execute()

ga.management().goals().insert(
      accountId=account_id,
      webPropertyId=webproperty_id,
      profileId=view_id,
      body={
          'id': 13,
          'active': True,
          'name': 'engage_videoview_50percent',
          'type': 'Event',
          'eventDetails':{
          'useEventValue': True,
          'eventConditions': [
      {
        "type": 'CATEGORY',
        "matchType": 'EXACT',
        "expression": 'engage'
      }, 
      {
        "type": 'ACTION',
        "matchType": 'EXACT',
        "expression": 'videoview'
      },
      {
        "type": 'VALUE',
        "comparisonType": 'GREATER_THAN',
        "comparisonValue": '50'
      }
    ]
      }
  } 
  ).execute()

ga.management().goals().insert(
      accountId=account_id,
      webPropertyId=webproperty_id,
      profileId=view_id,
      body={
          'id': 14,
          'active': True,
          'name': 'engage_videoview_75percent',
          'type': 'Event',
          'eventDetails':{
          'useEventValue': True,
          'eventConditions': [
      {
        "type": 'CATEGORY',
        "matchType": 'EXACT',
        "expression": 'engage'
      }, 
      {
        "type": 'ACTION',
        "matchType": 'EXACT',
        "expression": 'videoview'
      },
      {
        "type": 'VALUE',
        "comparisonType": 'GREATER_THAN',
        "comparisonValue": '75'
      }
    ]
      }
  } 
  ).execute()

ga.management().goals().insert(
      accountId=account_id,
      webPropertyId=webproperty_id,
      profileId=view_id,
      body={
          'id': 15,
          'active': True,
          'name': 'engage_videoview_complete',
          'type': 'Event',
          'eventDetails':{
          'useEventValue': True,
          'eventConditions': [
      {
        "type": 'CATEGORY',
        "matchType": 'EXACT',
        "expression": 'engage'
      }, 
      {
        "type": 'ACTION',
        "matchType": 'EXACT',
        "expression": 'videoview'
      },
      {
        "type": 'VALUE',
        "comparisonType": 'GREATER_THAN',
        "comparisonValue": '95'
      }
    ]
      }
  } 
  ).execute()
# audiences
#naming = ['Nuovi visitatori del sito negli ultimi 60gg', 'Utenti che hanno completato un video negli ultimi 60gg','Utenti che hanno aggiunto al carrello negli ultimi 30gg','Tutti gli utenti', 'Utenti che hanno visualizzato un prodotto negli ultimi 30gg', 'Utenti che sono ritornati sul sito negli ultimi 60gg', 'Utenti che hanno compilato un form negli ultimi 60gg','Utenti che hanno acquistato negli ultimi 30gg','Utenti che hanno iniziato il checkout negli ultimi 30gg']
#conditions = ['sessions::condition::ga:userType==New Visitor','sessions::condition::ga:goal9Completions>0', 'sessions::condition::ga:eventAction=@Add to Cart', '', 'sessions::condition::ga:goal1Completions>0','sessions::condition::ga:userType==Returning Visitor', 'sessions::condition::ga:goal10Completions>0', 'sessions::condition::ga:transactions>0', 'sessions::condition::ga:eventAction=@Checkout']
#duration = [60, 60, 30, 30, 30, 60, 60, 60, 30, 30] 
#i = 0
#while i<len(naming):
#    ga.management().remarketingAudience().insert(
#           accountId=account_id,
#           webPropertyId=webproperty_id,
#            body={
#          'name': naming[i],
#          'linkedViews': [view_id],
#          'linkedAdAccounts':[{
#          'type': 'ADWORDS_LINKS',
#           'linkedAccountId': '918-490-3917'
#           }],
#          'audienceType': 'SIMPLE',
#          'audienceDefinition': {
#          'includeConditions': {
#          'isSmartList': False,
#          'daysToLookBack': 7,
#          'membershipDurationDays': duration[i],
#          'segment': conditions[i]
#          }
#        }
#        }
#    ).execute()
#    i+=1

metrics = ['detailProductPrice','addProductPrice', 'removeProductPrice', 'checkoutTotalCart','revenueNET']
i = 0
while i<len(metrics):
  ga.management().customMetrics().insert(accountId=account_id, webPropertyId=webproperty_id, body={'name': metrics[i],'scope': 'HIT','type': 'CURRENCY','active': True}).execute()
  i+=1

dimensions = ['country','language', 'pageCategory', 'pageType', 'pagePath', 'scrollDepth','scrollDirection', 'videoTitle', 'videoStatus','videoVisible', 'videoViewPercent','formID', 'loggedStatus', 'userID','userMailMD5', 'fidelityNumber', 'dateRegistration','dateFirstPurchase','dateLastPurchase','deviceType']
i = 0
while i<len(dimensions):
  ga.management().customDimensions().insert(accountId=account_id, webPropertyId=webproperty_id, body={'name': dimensions[i],'scope': 'HIT','active': True}).execute()
  i+=1