import csv
import os
import io
import pandas as pd
import requests
import datetime
import time 
import logging
import sqlalchemy
from sqlalchemy import create_engine
from sqlalchemy.engine.url import URL
import settings_db
from google.oauth2 import service_account
from googleapiclient import discovery
from googleapiclient.http import MediaIoBaseDownload
from googleapiclient import http
SCOPES = ['https://www.googleapis.com/auth/dfareporting',
              'https://www.googleapis.com/auth/dfatrafficking',
              'https://www.googleapis.com/auth/ddmconversions']
pattern = os.path.join('/home/luigi/reporting/app/config/reporting-256219-d218faed9d76.json')
credentials = service_account.Credentials.from_service_account_file(
        pattern,scopes=SCOPES)
cm = discovery.build('dfareporting', 'v3.3',credentials=credentials)
request_user = cm.userProfiles().list()
response_user = request_user.execute()
profileId = response_user['items'][0]['profileId']
#logging.info("Number of profile IDs: {}".format(len(profileId)))
list_report = cm.reports().list(profileId=profileId).execute()
report_Id = list_report['items'][3]['id']
report_run = cm.reports().run(profileId=profileId, reportId=report_Id).execute()
fileId = report_run['id']
tries = 3
for i in range(tries):
    try:
       report_get = cm.files().get(reportId=report_Id, fileId=fileId).execute()
       time.sleep(2)
       if report_get['status'] == 'REPORT_AVAILABLE':
           filename = '/home/luigi/reporting/app/cm_api/file_output/gruppo_una.csv'
           report_directory = '/home/luigi/reporting/app/cm_api/file_output'
           filepath = os.path.join(report_directory, filename)
           out_file = io.FileIO(filepath, mode='wb')
           request = cm.files().get_media(reportId=report_Id, fileId=fileId)
           downloader = http.MediaIoBaseDownload(out_file, request)
           download_finished = False
           while download_finished is False:
               status, download_finished = downloader.next_chunk()
       else:
           raise Exception('still downloading. We are gonna try in 30 minutes times')
       break
    except:
        print("Timeout: sleep for 20 minutes and try again")
        time.sleep(300)
df = pd.read_csv('/home/luigi/reporting/app/cm_api/file_output/gruppo_una.csv', sep=',', quotechar = '"', header = None,  quoting=1, names=list(range(24)))
df = df.iloc[:-1]
df = df[~df[0].str.contains('Standard_query', na=False)]
df = df[~df[0].str.contains('Date/Time Generated', na=False)]
df = df[~df[0].str.contains('Report Time Zone', na=False)]
df = df[~df[0].str.contains('Account ID', na=False)]
df = df[~df[0].str.contains('Range', na=False)]
df = df[~df[0].str.contains('MRC Accredited Metrics', na=False)]
df = df[~df[0].str.contains('Fields', na=False)]
df = df[~df[0].str.contains('Search', na=False)]
new_header = df.iloc[0]
df = df[1:]
df.columns = new_header
df['Paid Search Advertiser ID']= df['Paid Search Advertiser ID'].astype(str).astype(int)
df['Paid Search Engine Account ID']= df['Paid Search Engine Account ID'].astype(str).astype(int)
df['Paid Search Campaign ID']= df['Paid Search Campaign ID'].astype(str).astype(int)
df['Paid Search Ad ID']= df['Paid Search Ad ID'].astype(str).astype(int)
df['Paid Search Keyword ID']= df['Paid Search Keyword ID'].astype(str).astype(int)
df['Paid Search Clicks']= df['Paid Search Clicks'].astype(str).astype(int)
df['Paid Search Impressions']= df['Paid Search Impressions'].astype(str).astype(int)
df['Paid Search Transactions']= df['Paid Search Transactions'].astype(str).astype(float)
df['Paid Search Transactions']= df['Paid Search Transactions'].astype(float).astype(int)
df['Paid Search Visits']= df['Paid Search Visits'].astype(str).astype(int)
df['Paid Search Actions']= df['Paid Search Actions'].astype(str).astype(float)
df['Paid Search Actions']= df['Paid Search Actions'].astype(float).astype(int)
df['Paid Search Cost']= df['Paid Search Cost'].astype(str).astype(float)
df['Paid Search Cost']= df['Paid Search Cost'].astype(str).astype(float)
df['Paid Search Revenue']= df['Paid Search Revenue'].astype(str).astype(float)
df['Paid Search Average Position']= df['Paid Search Average Position'].astype(str).astype(float)
df.columns = df.columns.str.replace('Paid Search ','')
df.columns = df.columns.str.replace(' ','_')
def connection():
     return create_engine(URL(**settings_db.DATABASE))
engine_live = connection().connect()
df.to_sql('temporary', con=engine_live, if_exists='replace', index= False, dtype={'Date':sqlalchemy.types.Date})
engine_live.execute("ALTER TABLE temporary add primary key(Date,Ad_ID,Keyword_ID)")
time.sleep(10)
engine_live.execute("INSERT INTO consolidate (Date, Advertiser, Advertiser_ID, Engine_Account, Engine_Account_ID, Campaign, Campaign_ID, Ad, Ad_Group, Ad_ID, Bid_Strategy, Bid_Strategy_ID, Keyword, Keyword_ID, Landing_Page_URL, Match_Type, Clicks, Impressions, Revenue, Average_Position, Transactions, Cost, Visits, Actions) SELECT Date, Advertiser, Advertiser_ID, Engine_Account, Engine_Account_ID, Campaign, Campaign_ID, Ad, Ad_Group, Ad_ID, Bid_Strategy, Bid_Strategy_ID, Keyword, Keyword_ID, Landing_Page_URL, Match_Type, Clicks, Impressions, Revenue, Average_Position, Transactions, Cost, Visits, Actions FROM temporary where NOT EXISTS (select * from consolidate where temporary.Date = consolidate.Date and temporary.Ad_ID = consolidate.Ad_ID and temporary.Keyword_ID = consolidate.Keyword_ID)")
engine_live.execute("UPDATE consolidate INNER JOIN temporary ON (temporary.Date = consolidate.Date and  temporary.Ad_ID = consolidate.Ad_ID and temporary.Keyword_ID = consolidate.Keyword_ID) SET temporary.Clicks = consolidate.Clicks and temporary.Impressions = consolidate.Impressions and temporary.Revenue = consolidate.Revenue and temporary.Average_Position = consolidate.Average_Position and temporary.Transactions = consolidate.Transactions and temporary.Cost = consolidate.Cost and temporary.Visits = consolidate.Visits and temporary.Actions = consolidate.Actions")
engine_live.close()
#df_export = df.to_csv('/home/luigi/reporting/app/cm_api/file_db_output/gruppo_una.csv',  sep=',', quotechar = '"',  quoting=1, index= False)
#from datetime import timedelta
#from datetime import datetime, time
#from datetime import date, timedelta
#path_def = '/home/luigi/reporting/app/cm_api/file_db_output'
#os.chdir(path_def)
#files_def = os.listdir(path_def)
#i = 'gruppo_una_search'
#from datetime import date, timedelta
#previous_x_day = date.today() - timedelta(days=1)
#previous_x_day = previous_x_day.strftime('%Y-%m-%d')
#for file in files_def:
#    os.rename(file, str(i)+'_'+previous_x_day+'.csv')