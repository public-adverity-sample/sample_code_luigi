from selenium import webdriver
from selenium.webdriver.support.ui import Select
import time
account =['sample', 'sample_1']
site =['wwww.sample.com', 'wwww.sample_2.com']

driver = webdriver.Chrome('/Users/luigilongo/Documents/Ga_man_api/bin/chromedriver')
time.sleep(3)
driver.get('https://tagmanager.google.com')
time.sleep(3)
driver.find_element_by_name('identifier').send_keys('')
time.sleep(3)
driver.find_element_by_xpath("//*[@id='identifierNext']/div").click()
time.sleep(5)
driver.find_element_by_name('password').send_keys('')
driver.find_element_by_xpath("//*[@id='passwordNext']/div").click()

def multiple_acccount(value_1,value_2):
	for (x,y) in zip(account,site):
		time.sleep(15)
		#click the create button in the GTM Home Page
		driver.find_element_by_xpath("/html/body/div[1]/div/div[2]/gtm-account-list/div/div/div/header/button").click()
		time.sleep(5)
		#loop through the account list 
		driver.find_element_by_name('form.account.data.name').send_keys(x)
		#select a country from the list. 75 Is the value for italy
		driver.find_element_by_xpath("/html/body/div[1]/div/div[2]/gtm-account-provision/gtm-admin-page/div/div/div[2]/div/div/div/content/form/div[1]/div/gtm-account-form/div/div[2]/select/option[75]").click()
		#loop through the site list 
		driver.find_element_by_name('form.container.data.name').send_keys(y)
		#select the Target platform. Choices are: web, app, etc
		driver.find_element_by_xpath("/html/body/div[1]/div/div[2]/gtm-account-provision/gtm-admin-page/div/div/div[2]/div/div/div/content/form/div[3]/gtm-container-form/div/div[2]/div/div[1]/div").click()
		time.sleep(5)
		#click the create button
		driver.find_element_by_xpath("/html/body/div[1]/div/div[2]/gtm-account-provision/gtm-admin-page/div/div/div[2]/div/div/div/content/form/button[1]").click()
		time.sleep(5)
		# check the Google Tag Manager Terms of Service Agreement
		driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[3]/div[3]/div[1]/gtm-checkbox/div/div").click()
		time.sleep(5)
		#click the Yes button for Google Tag Manager Terms of Service Agreement
		driver.find_element_by_xpath("/html/body/div[2]/div[2]/div[3]/div[1]/div[3]/button[2]").click()
		time.sleep(15)
		#click ok to close the pop-up containing the gtm javascript code
		driver.find_element_by_xpath("/html/body/div[6]/div/div/div[2]/div/div/div/button").click()
		time.sleep(5)
		#click on the gtm logo to go back to the home page and continue with the loop
		driver.find_element_by_xpath("//*[@id='suite-top-nav']/suite-header/div/md-toolbar/span/button[2]").click()
		time.sleep(14)

multiple_acccount(account,site)