from datetime import datetime
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
import numpy as np
import requests
from pprint import pprint
import pandas as pd
import pickle
def start_yesterday():
	from datetime import datetime, date, time, timedelta
	dt = datetime.combine(date.today(), time(0, 0, 0))
	sday_timestamp = None
	sday_timestamp = int((dt - timedelta(days=1)).timestamp())
	return sday_timestamp

lower_bound = start_yesterday()

def end_yesterday():
	from datetime import datetime, date, time, timedelta
	dt = datetime.combine(date.today(), time(0, 0, 0))
	eday_timestamp = None
	eday_timestamp = int((dt - timedelta(seconds=1)).timestamp())
	return eday_timestamp

upbound = end_yesterday()

def yesterda_dfa():
	from datetime import datetime, timedelta
	yesay_date = datetime.strftime(datetime.now() - timedelta(1), '%Y-%m-%d')
	return yesay_date

yesterday = yesterda_dfa()
#need to list users
payload = {'createdFrom': lower_bound ,'createdTo':upbound, 'token':'xxxxx'}
customer_call = requests.get('https://app.ecwid.com/api/v3/30601136/orders?', payload)
dictionary_json = customer_call.json()
pprint(dictionary_json)
type(dictionary_json)
#dictionary_orders= dictionary_json['items']
#pprint(dictionary_orders)
#len(dictionary_orders)
for key in dictionary_json.keys():
  print(key)

dictionary_json
b = ['items']
new_dict = {x: dictionary_json[x] for x in b}
for key in new_dict.keys():
  print(key)

lun_dict = len(new_dict)
pprint(new_dict)
list_dic = new_dict['items']
len(list_dic)
type(list_dic)
results =[]
i=0
while i<len(list_dic):
	dfo = list_dic[i]['items']
	results.append(pd.DataFrame(dfo))
	i+=1

dfa = pd.concat(results)
dropping = ['imageUrl','smallThumbnailUrl','hdThumbnailUrl','shortDescription','shortDescriptionTranslated','price','nameTranslated','taxes','selectedOptions','dimensions']
for x in dropping:	
	del dfa[x]

dfa.insert(0, 'Date', yesterday)
dfa['Date'] = pd.to_datetime(dfa['Date'], dayfirst=True)
dfa = dfa.reset_index(drop=True)
def export_data_first_block():
	dfa.to_pickle('/Users/luigilongo/Documents/ecwid_api/ecwid_step_1/step_1.pickle')

export_data_first_block()
dag = DAG('ecw_first_call', description='First_block_ecwid',
          schedule_interval='5 6 * * *',
          start_date=datetime(2017, 3, 20), catchup=False)
ecwid_operator = PythonOperator(task_id='ecwid_task', python_callable=export_data_first_block, dag=dag)
